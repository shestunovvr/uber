require('dotenv').config();
const {Router} = require('express');
const {getLoads,
  addLoad,
  getActiveLoad,
  iterateLoadState,
  getOneLoad,
  updateOneLoad,
  deleteOneLoad,
  postLoad,
  getLoadShippingInfo,
} = require('../controllers/loadControllers');
const authMiddleware = require('../middlewares/authMiddleware');
const driverCheckMiddleware = require('../middlewares/driverCheckMiddleware');
const shipperCheckMiddleware = require('../middlewares/shipperCheckMiddleware');
const router = new Router();

router.get('/', authMiddleware, getLoads);

router.post('/', shipperCheckMiddleware, addLoad);

router.get('/active', driverCheckMiddleware, getActiveLoad);

router.patch('/active/state', driverCheckMiddleware, iterateLoadState);

router.get('/:id', authMiddleware, getOneLoad);

router.put('/:id', shipperCheckMiddleware, updateOneLoad);

router.delete('/:id', shipperCheckMiddleware, deleteOneLoad);

router.post('/:id/post', shipperCheckMiddleware, postLoad);

router.get('/:id/shipping_info', shipperCheckMiddleware, getLoadShippingInfo);

module.exports = router;
