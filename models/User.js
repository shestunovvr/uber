const {Schema, model} = require('mongoose');

const schema = new Schema({
  email: {type: String, required: true, unique: true},
  password: {type: String, required: true},
  createdDate: {
    type: String,
    required: true,
    default: new Date().toLocaleString(),
  },
  role: {type: String, required: true},
});

module.exports = model('User', schema);
