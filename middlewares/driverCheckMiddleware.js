require('dotenv').config({path: '../.env'});
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const jwtSecret = process.env.jwtSecret;

const {
  userStatusInfo: {DRIVER},
} = require('../config/config');

const getUserById = async (id) => {
  return {
    user: await User.findOne(
        {_id: id},
        {
          __v: 0,
          password: 0,
        },
    ),
  };
};

const driverCheckMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      throw new Error();
    }
    const userInfo = jwt.verify(token, jwtSecret);
    const {user} = await getUserById(userInfo.userId);
    if (user.role.toLowerCase() !== DRIVER.toLowerCase()) {
      return res.status(400).json({
        message: 'This option is only available for drivers',
      });
    }
    req.user = {
      _id: user._id,
      role: user.role,
      email: user.email,
    };
    next();
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: 'User is not authorized',
    });
  }
};

module.exports = driverCheckMiddleware;
