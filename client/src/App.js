import './App.css';
import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import {useRoutes} from './Routes';
import {useAuth} from './hooks/authHook';
import {AuthContext} from './AuthContext';
// import {Navbar} from './components/Navbar'
// import {Loader} from './components/Loader'

function App() {
  const {token, login, logout, userId, ready} = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)

  return (
    <AuthContext.Provider value={{
      token, login, logout, userId, isAuthenticated
    }}>
      <Router>
        <div className="container">
          {routes}
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
