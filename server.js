require('dotenv').config();
const express = require('express');
const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');

const app = express();

const PORT = process.env.PORT || 8080;
const loggerFile = path.join(__dirname, 'logs.log');
try {
  if (!fs.existsSync(loggerFile)) {
    fs.appendFileSync(loggerFile, '', 'utf8');
  }
} catch (error) {
  console.log(error);
}

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), {
  flags: 'a',
});

app.use(morgan('combined', {stream: accessLogStream}));

app.use(cors());

app.use(express.json({extended: true}));

app.use('/api/auth', require('./routes/authRoutes'));
app.use('/api/users', require('./routes/userRoutes'));
app.use('/api/trucks', require('./routes/truckRoutes'));
app.use('/api/loads', require('./routes/loadRoutes'));

if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static(path.join(__dirname, 'client', 'build')));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

(async () => {
  try {
    await mongoose.connect(process.env.mongoUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    app.listen(PORT, () =>
      console.log(`App has been started on port ${PORT}...`),
    );
  } catch (e) {
    console.log('Server Error', e.message);
    process.exit(1);
  }
})();
