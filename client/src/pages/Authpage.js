import React, {useContext, useEffect, useState} from 'react';
import {useHttp} from '../hooks/httpHook';
import {AuthContext} from '../AuthContext';

export const AuthPage = () => {
  const auth = useContext(AuthContext);
  const {loading, request, error, clearError} = useHttp();
  const [form, setForm] = useState({
    email: '', password: '', role: ''
  })

//   useEffect(() => {
//     window.M.updateTextFields()
//   }, [])

  const changeHandler = event => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }

  const registerHandler = async () => {
    try {
      const data = await request('/api/auth/register', 'POST', {...form});
    } catch (e) {}
  }

  const loginHandler = async () => {
    try {
      const data = await request('/api/auth/login', 'POST', {...form});
      console.log(data.jwt_token);
      auth.login(data.jwt_token);
    } catch (e) {}
  }

  return (
    <div className="auth-cont">
      <div className="auth-inner-cont">
        <h1>Type in your creds</h1>
        <div className="card blue darken-1">
          <div className="card-content white-text">
            <div>
              <div className="input-field">
                <input
                  placeholder="Введите email"
                  id="email"
                  type="text"
                  name="email"
                  className="yellow-input"
                  value={form.email}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field">
                <input
                  placeholder="Введите пароль"
                  id="password"
                  type="password"
                  name="password"
                  value={form.password}
                  onChange={changeHandler}
                />
                <label htmlFor="email">Пароль</label>
              </div>
              <div className="input-field">
                <input
                  placeholder="role"
                  id="role"
                  type="text"
                  name="role"
                  value={form.role}
                  onChange={changeHandler}
                />
                <label htmlFor="role">Role</label>
              </div>

            </div>
          </div>
          <div className="card-action">
            <button
              className="btn yellow darken-4"
              style={{marginRight: 10}}
              disabled={loading}
              onClick={loginHandler}
            >
              Sign in
            </button>
            <button
              className="btn grey lighten-1 black-text"
              onClick={registerHandler}
              disabled={loading}
            >
              Sign up
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}