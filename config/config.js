const userRoles = ['DRIVER', 'SHIPPER'];
const userStatusInfo = {
  DRIVER: 'DRIVER',
  SHIPPER: 'SHIPPER',
};
const truckStatusList = ['IS', 'OL'];
const truckTypeList = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];

const truckInfo = {
  'SPRINTER': {
    payload: 1700,
    dimensions: {
      length: 300,
      width: 250,
      height: 170,
    },
  },
  'SMALL STRAIGHT': {
    payload: 2500,
    dimensions: {
      length: 500,
      width: 250,
      height: 170,
    },
  },
  'LARGE STRAIGHT': {
    payload: 4000,
    dimensions: {
      length: 700,
      width: 350,
      height: 200,
    },
  },
};


const loadStatusList = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];
const loadStateList = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];


module.exports = {
  userRoles,
  truckStatusList,
  truckTypeList,
  userStatusInfo,
  truckInfo,
  loadStateList,
  loadStatusList,
};
