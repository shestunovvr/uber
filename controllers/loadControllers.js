require('dotenv').config({path: '../.env'});
const ObjectId = require('mongoose').Types.ObjectId;
const {
  truckInfo,
  loadStateList,
  loadStatusList,
  truckStatusList,
} = require('../config/config');
const Truck = require('../models/Truck');
const User = require('../models/User');
const Load = require('../models/Load');

const getLoads = async (req, res) => {
  try {
    const user = await User.findOne(
        {_id: req.user._id},
        {
          __v: 0,
          password: 0,
        },
    );
    const allLoads = await Load.find();
    if (user.role === 'DRIVER') {
      const driverLoads = [];
      allLoads.map((load) => {
        if (load.assigned_to === req.user._id.toString()) {
          driverLoads.push(load);
        }
      });
      res.status(200).json({
        loads: driverLoads,
      });
    } else {
      const shipperLoads = [];
      allLoads.map((load) => {
        if (load.created_by === req.user._id) {
          shipperLoads.push(load);
        }
      });
      res.status(200).json({
        loads: shipperLoads,
      });
    }
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const addLoad = async (req, res) => {
  try {
    const load = new Load({
      created_by: new ObjectId(req.user._id),
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: req.body.dimensions,
      logs: [
        {
          message: 'Load created sussessfully with status NEW',
          time: new Date().toLocaleString(),
        },
      ],
    });
    await load.save();
    res.status(200).json({
      message: 'Load created successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const getActiveLoad = async (req, res) => {
  try {
    const load = await Load.findOne(
        {assigned_to: req.user._id.toString(), status: loadStatusList[2]},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(200).json({
        load: {},
      });
    }
    res.status(200).json({
      load,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const iterateLoadState = async (req, res) => {
  try {
    const myTruck = await Truck.findOne(
        {assigned_to: req.user._id.toString()},
        {
          __v: 0,
        },
    );
    const load = await Load.findOne(
        {assigned_to: req.user._id.toString(), status: loadStatusList[2]},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No active loads for you found',
      });
    }
    const currentStateIndex = loadStateList.indexOf(load.state);
    if (currentStateIndex === 2) {
      load.state = loadStateList[3];
      myTruck.status = truckStatusList[0];
      load.status = loadStatusList[3];
      await myTruck.save();
    } else {
      load.state = loadStateList[currentStateIndex + 1];
    }
    load.logs.push({
      message: `Load state changed to ${load.state}`,
      time: new Date().toLocaleString(),
    });
    await load.save();
    res.status(200).json({
      message: `Load state changed to ${load.state}`,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const getOneLoad = async (req, res) => {
  try {
    const load = await Load.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No load with this ID found',
      });
    }
    res.status(200).json({
      load,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const updateOneLoad = async (req, res) => {
  try {
    const load = await Load.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No load with this ID found',
      });
    }
    if (load.status !== 'NEW') {
      return res.status(400).json({
        message: 'You can delete only new notes',
      });
    }
    load.logs.push({
      message: 'Load details updated successfully',
      time: new Date().toLocaleString(),
    });

    await load.save();
    await Load.findOneAndUpdate({_id: req.params.id}, {$set: req.body});

    res.status(200).json({
      message: 'Load details changed successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const deleteOneLoad = async (req, res) => {
  try {
    const load = await Load.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No load with this ID found',
      });
    }
    if (load.status !== 'NEW') {
      return res.status(400).json({
        message: 'You can delete only new notes',
      });
    }
    await Load.deleteOne({_id: req.params.id});
    res.status(200).json({
      message: 'Load deleted successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const postLoad = async (req, res) => {
  try {
    const load = await Load.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No load with this ID found',
      });
    }
    if (load.status !== loadStatusList[0]) {
      return res.status(400).json({
        message: 'You can assign only new notes',
      });
    }
    load.status = loadStatusList[1];
    const trucks = await Truck.find();
    const assignedTrucks = trucks.filter(
        (truck) => truck.assigned_to[0] > 1,
    );
    if (assignedTrucks.length < 1) {
      load.status = loadStatusList[0];
      load.logs.push({
        message: 'Unsuccessully tried to find a driver',
        time: new Date().toLocaleString(),
      });
      await load.save();
      return res.status(400).json({
        message: 'There is no assigned trucks',
      });
    }

    const allLoads = await Load.find();
    const loadTrucks = [];
    allLoads.forEach((load) => {
      if (load.status !== loadStatusList[3]) {
        loadTrucks.push(load.assigned_to);
      }
    });
    assignedTrucks.forEach((truck) => {
      if (!loadTrucks.includes(truck._id.toString())) {
        if (
          truck.status === 'IS' &&
                    truckInfo[truck.type].dimensions.length >=
                        load.dimensions.length &&
                    truckInfo[truck.type].dimensions.width >=
                        load.dimensions.width &&
                    truckInfo[truck.type].dimensions.height >=
                        load.dimensions.height &&
                    truckInfo[truck.type].payload >= load.payload
        ) {
          load.status = loadStatusList[2];
          load.state = loadStateList[0];
          load.assigned_to = truck.assigned_to.toString();
        }
      }
    });
    if (load.assigned_to < 1) {
      load.status = loadStatusList[0];
      load.logs.push({
        message: 'Unsuccessully tried to find a driver',
        time: new Date().toLocaleString(),
      });
      await load.save();
      return res.status(400).json({
        message: 'There is no assigned trucks',
      });
    }
    const assignedTruck = await Truck.findOne(
        {assigned_to: load.assigned_to},
        {
          __v: 0,
        },
    );
    assignedTruck.status = truckStatusList[1];
    await assignedTruck.save();
    load.logs.push({
      message: `Load assigned to driver with ID 
      ${assignedTruck.assigned_to.toString()}`,
      time: new Date().toLocaleString(),
    });
    await load.save();
    return res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (e) {
    load.status = loadStatusList[0];
    await load.save();
    res.status(500).json({
      message: e.message,
    });
  }
};

const getLoadShippingInfo = async (req, res) => {
  try {
    const load = await Load.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!load) {
      return res.status(400).json({
        message: 'No load with this ID found',
      });
    }
    const assignedTruck = await Truck.findOne(
        {assigned_to: load.assigned_to},
        {
          __v: 0,
        },
    );
    res.status(200).json({
      load: load,
      truck: assignedTruck,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  iterateLoadState,
  getOneLoad,
  updateOneLoad,
  deleteOneLoad,
  postLoad,
  getLoadShippingInfo,
};
