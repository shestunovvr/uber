require('dotenv').config();
const {Router} = require('express');
const {getUser,
  deleteUser,
  changePassword,
  getAllUsers,
} = require('../controllers/userControllers');
const authMiddleware = require('../middlewares/authMiddleware');
const router = new Router();

router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me/password', authMiddleware, changePassword);

router.get('/all', getAllUsers);

module.exports = router;
