require('dotenv').config({path: '../.env'});
const bcrypt = require('bcrypt');
const User = require('../models/User');

const deleteUser = async (req, res) => {
  try {
    await User.deleteOne({_id: req.user._id});
    res.status(200).json({
      message: 'Profile deleted successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const getUser = async (req, res) => {
  try {
    const user = await User.findOne(
        {_id: req.user._id},
        {
          __v: 0,
          password: 0,
        },
    );
    res.status(200).json({
      user,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const changePassword = async (req, res) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findOne({_id: req.user._id});
    if (!user) {
      res.status(500).json({
        message: 'User not found',
      });
    }
    const checkOldPassword = await bcrypt.compare(oldPassword, user.password);
    if (!checkOldPassword) {
      res.status(400).json({
        message: 'Wrong password',
      });
    }
    user.password = await bcrypt.hash(newPassword, 12);
    await user.save();
    res.status(200).json({
      message: 'Password changed successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: 'Something gone wrong, try again',
    });
  }
};

const getAllUsers = async (req, res) => {
  try {
    const users = await User.find();
    const usersIDs = [];
    users.forEach((el) => usersIDs.push(el._id));

    console.log('users: ', usersIDs);
    res.status(200).json({
      usersIDs,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

module.exports = {getUser, deleteUser, changePassword, getAllUsers};
