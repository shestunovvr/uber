require('dotenv').config({path: '../.env'});
const jwt = require('jsonwebtoken');

const jwtSecret = process.env.jwtSecret;

const authMiddleware = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      throw new Error();
    }
    const userInfo = jwt.verify(token, jwtSecret);
    req.user = {
      _id: userInfo.userId,
      role: userInfo.role,
      email: userInfo.email,
    };
  } catch (error) {
    console.log(error);
    const err = new Error('User not authorized');
    err.status = 400;
    throw err;
  }
  next();
};

module.exports = authMiddleware;
