require('dotenv').config();
const {Router} = require('express');
const {getTrucks,
  addTruck,
  getOneTruck,
  updateOneTruck,
  deleteOneTruck,
  assignOneTruck,
} = require('../controllers/truckControllers');
const driverCheckMiddleware = require(
    '../middlewares/driverCheckMiddleware',
);
const router = new Router();

router.get('/', driverCheckMiddleware, getTrucks);

router.post('/', driverCheckMiddleware, addTruck);

router.get('/:id', driverCheckMiddleware, getOneTruck);

router.put('/:id', driverCheckMiddleware, updateOneTruck);

router.delete('/:id', driverCheckMiddleware, deleteOneTruck);

router.post('/:id/assign', driverCheckMiddleware, assignOneTruck);

module.exports = router;
