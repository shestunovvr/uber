require('dotenv').config();
const {Router} = require('express');
const {
  register,
  login,
  forgotPassword,
} = require('../controllers/authControllers');
const {check} = require('express-validator');
const router = new Router();

router.post(
    '/register',
    [
      check('email', 'Wrong email').isEmail(),
      check('password', 'Minimum length is 8 symbols').isLength({min: 8}),
    ],
    register,
);

router.post(
    '/login',
    [
      check('email', 'Type email in email format').normalizeEmail().isEmail(),
      check('password', 'Type password').exists(),
    ],
    login,
);

router.post(
    '/forgot_password',
    [
      check('email', 'Type email in email format').normalizeEmail().isEmail(),
      check('password', 'Type password').exists(),
    ],
    forgotPassword,
);

module.exports = router;
