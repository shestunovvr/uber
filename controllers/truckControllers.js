require('dotenv').config({path: '../.env'});
const ObjectId = require('mongoose').Types.ObjectId;
const {truckTypeList} = require('../config/config');
const Truck = require('../models/Truck');
const User = require('../models/User');

const getTrucks = async (req, res) => {
  try {
    const trucks = await Truck.find({created_by: req.user._id},
        {
          __v: 0,
        });
    res.status(200).json({
      trucks: trucks,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const addTruck = async (req, res) => {
  try {
    if (!truckTypeList.includes(req.body.type.toUpperCase())) {
      throw new Error('Invalid truck type');
    }
    const truck = new Truck({
      created_by: new ObjectId(req.user._id),
      type: req.body.type.toUpperCase(),
    });

    await truck.save();

    res.status(200).json({
      message: 'Truck created successfully',
    });
  } catch (e) {
    console.log('outer catch');
    res.status(500).json({
      message: e.message,
    });
  }
};

const getOneTruck = async (req, res) => {
  try {
    const truck = await Truck.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!truck) {
      res.status(200).json({
        message: 'No truck with this ID found',
      });
    }
    res.status(200).json({
      truck,
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const updateOneTruck = async (req, res) => {
  try {
    const truck = await Truck.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!truck) {
      res.status(200).json({
        message: 'No truck with this ID found',
      });
    }
    const users = await User.find();
    const usersIDs = [];
    users.forEach((el) => usersIDs.push(el._id.toString()));
    if (usersIDs.includes(truck.assigned_to)) {
      return res.status(400).json({
        message: 'This truck is already assigned to another driver',
      });
    }
    if (!truckTypeList.includes(req.body.type)) {
      throw new Error('Invalid truck type');
    }
    truck.type = await req.body.type;
    await truck.save();
    res.status(200).json({
      message: 'Truck details changed successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const deleteOneTruck = async (req, res) => {
  try {
    const truck = await Truck.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!truck) {
      res.status(200).json({
        message: 'No truck with this ID found',
      });
    }
    const users = await User.find();
    const usersIDs = [];
    users.forEach((el) => usersIDs.push(el._id.toString()));
    if (usersIDs.includes(truck.assigned_to)) {
      return res.status(400).json({
        message: 'This truck is already assigned to another driver',
      });
    }
    await Truck.deleteOne({_id: req.params.id});
    res.status(200).json({
      message: 'Truck deleted successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};

const assignOneTruck = async (req, res) => {
  try {
    const truckToAssign = await Truck.findOne(
        {_id: req.params.id},
        {
          __v: 0,
        },
    );
    if (!truckToAssign) {
      return res.status(400).json({
        message: 'No truck with this ID found',
      });
    }
    const users = await User.find();
    const usersIDs = [];
    users.forEach((el) => usersIDs.push(el._id.toString()));
    const trucks = await Truck.find();
    const assignedTrucks = [];
    trucks.forEach((truck) => assignedTrucks.push(truck.assigned_to));
    if (assignedTrucks.includes(req.user._id.toString())) {
      return res.status(400).json({
        message: 'This driver already assigned with another truck',
      });
    }
    if (usersIDs.includes(truckToAssign.assigned_to)) {
      return res.status(400).json({
        message: 'This truck is already assigned to another driver',
      });
    }
    truckToAssign.assigned_to = req.user._id;
    await truckToAssign.save();
    res.status(200).json({
      message: 'Truck assigned successfully',
    });
  } catch (e) {
    res.status(500).json({
      message: e.message,
    });
  }
};
module.exports = {
  getTrucks,
  addTruck,
  getOneTruck,
  updateOneTruck,
  deleteOneTruck,
  assignOneTruck,
};
