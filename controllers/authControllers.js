require('dotenv').config({path: '../.env'});
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/User');
const {userRoles} = require('../config/config');

const jwtSecret = process.env.jwtSecret;

const register = async (req, res) => {
  try {
    const {email, password, role} = req.body;

    const candidate = await User.findOne({email: email.toLowerCase()});

    if (candidate) {
      return res.status(400).json({message: 'User already exists'});
    }
    if (!userRoles.includes(role)) {
      return res.status(400).json({
        message: 'Choose the right role',
      });
    }
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({
      role: role.toUpperCase(),
      email: email.toLowerCase(),
      password: hashedPassword,
    });

    await user.save();

    res.status(200).json({message: 'Profile created successfully'});
  } catch (e) {
    res.status(500).json({
      message: 'Something gone wrong, try again',
    });
  }
};

const login = async (req, res) => {
  try {
    const {email, password} = req.body;

    const user = await User.findOne({email: email.toLowerCase()});

    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).json({message: 'Wrong password'});
    }

    const token = jwt.sign(
        {userId: user.id},
        jwtSecret,
        {expiresIn: 86400},
    );

    res.status(200).json({jwt_token: token});
  } catch (e) {
    res.status(500).json({
      message: 'Something gone wrong, try again',
    });
  }
};

const forgotPassword = async (req, res) => {
  try {
    const {email} = req.body;

    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({message: 'User not found'});
    }

    res.status(200).json({message: 'New password sent to your email address'});
  } catch (e) {
    res.status(500).json({
      message: 'Something gone wrong, try again',
    });
  }
};

module.exports = {register, login, forgotPassword};
