const {Schema, model} = require('mongoose');
const {loadStatusList, loadStateList} = require('../config/config');

const schema = new Schema({
  name: {type: String, required: true},
  created_by: {type: String, required: true},
  status: {
    type: String,
    enum: loadStatusList,
    required: true,
    default: loadStatusList[0],
  },
  assigned_to: {type: String, default: ''},
  state: {type: String, enum: loadStateList, default: loadStateList[0]},
  payload: {type: Number, default: 0},
  pickup_address: {type: String, required: true},
  delivery_address: {type: String, required: true},
  dimensions: {width: Number, length: Number, height: Number},
  createdDate: {
    type: String,
    required: true,
    default: new Date().toLocaleString(),
  },
  logs: {
    type: [
      {
        message: {type: String, required: true},
        time: {type: String, default: new Date().toLocaleString()},
      },
    ],
  },
});

module.exports = model('Load', schema);
