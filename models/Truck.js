const {Schema, model} = require('mongoose');

const schema = new Schema({
  created_by: {type: String, required: true},
  type: {type: String, required: true},
  assigned_to: {type: String, default: ''},
  status: {type: String, default: 'IS'},
  createdDate: {
    type: String,
    required: true,
    unique: true,
    default: new Date().toLocaleString(),
  },
});

module.exports = model('Truck', schema);
