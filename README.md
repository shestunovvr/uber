Hello and welcome to my UBER-like application.
At the moment it's only back-end part of it running at port 8080 (you can change it in the .env file).

You can run app in 2 commands:
npm install
npm start

Also you can you use 'npm run nodemon' for more comfortable development.
